﻿window.addEventListener('DOMContentLoaded', function () {

	const canvas = document.getElementById("renderCanvas");
	let engine = null;
	let scene = null;

	const startRenderLoop = function (engine) {
		engine.runRenderLoop(function () {
			if (scene && scene.activeCamera) {
				scene.render();
			}
		});
	}

	let createDefaultEngine = function () {
		return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true, disableWebGL2Support: false });
	};

	let initFunction = async function () {
		let asyncEngineCreation = async function () {
			try {
				return createDefaultEngine();
			} catch (e) {
				console.log("The available createEngine function failed. Creating the default engine instead");
				return createDefaultEngine();
			}
		}

		engine = await asyncEngineCreation();
		if (!engine) throw 'engine should not be null.';
		startRenderLoop(engine);
		scene = createScene(engine);
		
		window.addEventListener("resize", function () {
			engine.resize();
		});

	};

	initFunction();

	// Resize
	let createScene = function (engine) {
		let scene = new BABYLON.Scene(engine);

		let camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 15, -100), scene);
		camera.setTarget(BABYLON.Vector3.Zero());
		camera.attachControl(canvas, true);

		let light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), scene);
		light.intensity = 0.7;

		let blueMaterial = new BABYLON.StandardMaterial("blueMaterial", scene);
		blueMaterial.diffuseColor = new BABYLON.Color3(0, 0, 1);

		let redMaterial = new BABYLON.StandardMaterial("redMaterial", scene);
		redMaterial.diffuseColor = new BABYLON.Color3(1, 0, 0);


		let r = 60
		let redGround = BABYLON.MeshBuilder.CreateBox("redGround", { width: r, height: 0.1, depth: r });
		redGround.position.x = -35;
		redGround.material = redMaterial;

		let blueGround = BABYLON.MeshBuilder.CreateBox("blueGround", { width: r, height: 0.1, depth: r });
		blueGround.position.x = 35;
		blueGround.material = blueMaterial;

		let d = 10
		let blueBox = BABYLON.MeshBuilder.CreateBox("blueBox", { size: d });
		blueBox.position.x = -((r + d) / 2);
		blueBox.position.y = d / 2;
		blueBox.material = blueMaterial

		let redSphere = BABYLON.MeshBuilder.CreateSphere("redSphere", { diameter: d, segments: 32 });
		redSphere.position.x = (r + d) / 2;
		redSphere.position.y = d / 2;
		redSphere.material = redMaterial;

		return scene;
	};

});
